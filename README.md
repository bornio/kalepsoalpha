# KalepsoDB
KalepsoDB is a Java library that allows you to securely store databases, while retaining search and retrieval functionality.
This Alpha Test Realm is the first step in bringing to life the next-level of secure searchable encryption. 
Using this library, your data is dynamically re-encrypted and robustly obfuscated, while you keep functionality, such as searching and updating your data. 
This ensures that no current or future attacks to the database can compromise your data privacy.

# Download and Dependencies
In order to use Kalepso, you need Java 8 and three files: 

1. Kalepso.jar, which has dependencies on the following two files

2. Spring Security Crypto 5.0.0: https://mvnrepository.com/artifact/org.springframework.security/spring-security-crypto

3. Colt 1.2.0: https://mvnrepository.com/artifact/colt/colt

All you need to do is to download these three jar files and include them into your Java project. 

# Limitations
The Kalepso library currently has the following limitations:

1. Attributes must be indexed before performing a search on them

2. Maximum number of records: 24,576

3. Maximum number of tables: 10

4. Maximum number of index updates: 256

5. Maximum record size: 512 bytes

# Basic Usage
Before using the Kalepso library, you need to connect to our server using the ip address provided by us.
```java
	    //connect to Kalepso
	    Kalepso kalepso = Kalepso.connect("IP_ADDRESS");
```
Once connected, you can create a new table in your database. For each column of the table, a new Attribute object has to be created which specifies the attribute name, its datatype, and its bounds (minimum and maximum values) for numeric attributes (e.g., Age can be from 0 to 100), or a list of string values for string attributes that take only specific values (e.g., Married can be true or false), or no bounds for string values which are not predefined (e.g., Name can be anything). All Attribute objects have to be added to an ArrayList with which the createTable() function can be called in order to create the new table.
```java
        //create a new table
        ArrayList<Attribute> tableStructure = new ArrayList<>();
        tableStructure.add(new Attribute("Name", java.lang.String.class));
	    tableStructure.add(new Attribute("Age", java.lang.Integer.class, 0 , 100));
	    tableStructure.add(new Attribute("Salary", java.lang.Integer.class, 0, 500000));
	    tableStructure.add(new Attribute("Married", java.lang.Boolean.class, new ArrayList<String>(Arrays.asList(new String[]{"true","false"}))));
	    kalepso.createTable("exampleTable", tableStructure);
```
Now that you have a table in the database, you can index attributes that you want to search for later on. Kalepso is the only available tech allowing you to index attributes that are encrypted.
In order to index an attribute, the createIndex() function can be called. This function takes as parameters a list of Strings, which specify the names of the attributes that should be indexed and the table name.
```java
      //index the attributes you want to search for
        ArrayList<String> indexedAttributes = new ArrayList<>();
	    indexedAttributes.add("Age");
	    indexedAttributes.add("Married");
	    indexedAttributes.add("Salary");
	    kalepso.createIndex(indexedAttributes, "exampleTable");
```
Having created the table and the indices, you can now add records to the table. 
A record is an object array, which contains a value for each column. All records can be added to an ArrayList, which can then be added to the table by using the add() function.
```java
        //add data to your table
        ArrayList<Object[]> records = new ArrayList<>();
        Object[] row1 = {"exampleName1", 20, 300000, true};
        Object[] row2 = {"exampleName2", 50, 400000, false};
        Object[] row3 = {"exampleName3", 40, 800000, true};

        records.add(row1);
        records.add(row2);
        records.add(row3);
        kalepso.add(records, "exampleTable");
```
You can now create and perform queries. 
In order to create a query for an attribute of number datatype (integer, double, float...), you can call the numQuery() function and specify the range of values, for which you want to search.
To search for a Boolean or String attribute, you can call the stringQuery() function and specify the attribute name and the value you want to search for.
All query objects can be added to an ArrayList. 
If the issueQuery() function is called with this ArrayList, all queries will be performed at once. The matching records will be stored in an ArrayList of Object arrays.
```java
        //create query: return all records with Age from 20 to 70, Salary from 100000 to 400000, Married = true and Name = exampleName1
        ArrayList<Query> query = new ArrayList<>();
        query.add(kalepso.numQuery("Age", 20, 70, "exampleTable"));
        query.add(kalepso.numQuery("Salary", 200000, 300000, "exampleTable"));
        query.add(kalepso.stringQuery("Married", "true", "exampleTable"));
        query.add(kalepso.stringQuery("Name", "exampleName1", "exampleTable"));

        //execute query
        HashMap<String, ArrayList<Object[]>> result = (HashMap)kalepso.issueQuery(query);

        //get all table names
        ArrayList<String> tableList = new ArrayList<String>(result.keySet());


        //print retrieved records
            for(int k = 0; k < tableList.size(); k++)
            {
                System.out.println("----------"+tableList.get(k)+"----------");
                for(int i = 0; i < result.get(tableList.get(k)).size(); i++)
                {
                    for(int j = 0; j < result.get(tableList.get(k)).get(i).length; j++)
                    {
                        System.out.print(result.get(tableList.get(k)).get(i)[j] + " ");
                    }
                    System.out.println();
                }
            }

```
After everything is done, you can disconnect from Kalepso.
```java
		Kalepso.disconnect();
```

#### Remarks
1. In order to delete a record from a table, the deleteRecord() function can be called. 
This function takes as input the ID of the record and the name of the table which contains the record. 
Whenever a new record is added to a table, it is assigned a unique ID, which is set as the first entry of each record. This ID is required in order to delete or update a record.

2. When you disconnect from Kalepso, all structures that you created will be lost. 
In order to save these structures, you can call the saveSession() function. 
This function takes a String as input, specifying the name of the session. If you want to continue with this session at a later point, you can call the loadSession() function, giving the name of the session as input that you want to resume from.

# Test API
For a quick test of the KalepsoDB library, you can also use our Test API. We created a simple test case in this API which requires only a small amount of programming:

```java
        Kalepso kalepso = Kalepso.connect("IP_ADDRESS");
        
        Test.initializeDB(kalepso);
        
        Test.generateData(kalepso);
        
        ArrayList<Query> query = Test.query(kalepso);
        
        Test.performQuery(kalepso, query);
        
        Kalepso.disconnect();
```

# Connecting to Redis
In order to access the redis server, you need to do the following:

1. Install a redis client on your device (we suggest redis-cli)

2. Connect to the redis server using the IP address and password porvided by us (e.g. redis-cli -h "ip_address" -a "password")

3. Once connected to the redis server, you can look at records stored on the server (e.g. get 0#0) 

4. The redis server is pre-loaded with random data. Please do *not* delete or alter any values, otherwise the system will stop working properly